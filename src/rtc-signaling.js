import { LitElement, html } from "lit";
import { signalingStyles } from "./styles/signaling.js";
import { SIGNALING_METHODS } from "./utils.js";

export default class RTCSignaling extends LitElement {
	static properties = {
		signalsOut: { type: Array, state: true },
		signalsIn: { type: Array, state: true },
		sharableSignals: { type: Array, state: true },
		methods: { type: Array, state: true },
		method: { type: String, state: true },
		peerUser: { type: Object, state: true },
		matrixPeers: { type: Array, state: true },
	};
	constructor() {
		super();
		this.signalsOut = [];
		this.signalsIn = [];
		this.sharableSignals = ["offerbundle", "answerbundle"];
		this.methods = [];
		this.method = "";
		this.matrixPeers = [];
	}
	connectedCallback() {
		super.connectedCallback();
		this.methods = this.methods || SIGNALING_METHODS;
		this.method = this.methods[0];
	}

	/* this event does not go outside,
		 only to our local peer to instanciate an offer */
	offer() {
		const offerConnection = new CustomEvent("offer", {
			bubbles: true,
		});
		this.dispatchEvent(offerConnection);
	}

	/* a public method, used by our peer to signal itself to the world */
	async signalOut(signal) {
		this.signalsOut = [signal, ...this.signalsOut];
		/* if (this.userPeer.matrix_user_id) {
			 matrixApi.sendToDevice({
			 user_id: matrix_user_id,
			 content: signal
			 })
			 } */
		const signalOutEvent = new CustomEvent("signalOut", {
			bubbles: true,
			detail: signal,
		});
		this.dispatchEvent(signalOutEvent);
	}

	/* a public method, to signal to our peer,
		 that we received incoming peer data from the outside world */
	async signalIn(signal) {
		this.signalsIn = [signal, ...this.signalsIn];
		const signalInEvent = new CustomEvent("signalIn", {
			bubbles: true,
			detail: signal,
		});
		this.dispatchEvent(signalInEvent);
	}

	_onOffer({ target }) {
		target.setAttribute("disabled", true);
		this.offer();
	}

	_onReadPeer({ detail }) {
		this.signalIn(detail);
	}
	_onMethod({ target }) {
		this.method = target.value;
	}

	render() {
		return html`
			<menu part="menu">
				<li part="config">${this._renderMethods()}</li>
				<li part="out" length=${this.signalsOut.length}>
					${this._renderOfferConnection()}
					${this._renderSignalsOut(this.signalsOut)}
				</li>
				<li part="in" length=${this.signalsIn.length}>
					<rtc-signaling-read
						@peer=${this._onReadPeer}
						?auto-connect=${this.autoConnect}
						method=${this.method}
						.matrixPeers=${this.matrixPeers}
					></rtc-signaling-read>
					${false ? this._renderSignalsIn(this.signalsIn) : ""}
				</li>
			</menu>
		`;
	}
	_renderMethods() {
		if (this.methods.length > 1) {
			return html`
				<select
					@change=${this._onMethod}
					.selected=${this.method}
					part="methods"
					title="Select which method to use for signaling in & out"
				>
					${this.methods.map(this._renderMethod)}
				</select>
			`;
		} else {
			return html`<code>${this.method}</code>`;
		}
	}
	_renderMethod(method) {
		return html` <option value=${method}>${method}</option> `;
	}
	_renderOfferConnection() {
		const offerConnectionButton = html`
			<button
				@click=${this._onOffer}
				part="offer"
				title="Create a WebRTC peer connection offer, to be shared with an other peer, in other to connect without intermediary."
			>
				offer connection
			</button>
		`;
		if (this.signalsOut.length || this.signalsOut.length) {
			return "";
		} else if (!this.signalsOut.length && !this.signalsIn.length) {
			return offerConnectionButton;
		}
	}
	_renderSignalsOut(signals) {
		if (signals && signals.length) {
			return html`
				<ul part="signals-out">
					${signals
						.slice(0, 1)
						.map(
							(s) => html`<li part="signal-out">${this._renderSignal(s)}</li>`,
						)}
				</ul>
			`;
		}
	}
	_renderSignalsIn(signals) {
		if (signals && signals.length) {
			return html`
				<ul part="signals-in">
					${signals
						.slice(0, 1)
						.map(
							(s) => html`<li part="signal-in">${this._renderSignal(s)}</li>`,
						)}
				</ul>
			`;
		}
	}
	_renderSignal(signal) {
		const isSharable = this.sharableSignals.indexOf(signal.type) > -1;
		const $share = html`
			<rtc-signaling-share
				.data=${signal}
				.userPeer=${this.userPeer}
				.matrixPeers=${this.matrixPeers}
				method=${this.method}
			></rtc-signaling-share>
		`;
		return html`${isSharable ? $share : signal.type} `;
	}

	static styles = [signalingStyles];
}
