import { LitElement, html } from "lit";
import { buttonStyles } from "./styles/form.js";

export default class RTCSignalingCopy extends LitElement {
	static properties = {
		data: { type: String },
		text: { type: String },
	};
	constructor() {
		super();
		this.data = this.getAttribute("data");
		this.text = `↻ copy`;
	}
	onCopy({ target }) {
		navigator.clipboard.writeText(this.data);
		target.innerText = "copied";
		globalThis.setTimeout(() => {
			target.innerText = this.text;
		}, 555);
	}
	render() {
		return html`
			<button
				@click=${this.onCopy}
				title="Copy the data to be sent to the other peer, for pasting"
			>
				${this.text}
			</button>
		`;
	}
	static styles = [buttonStyles];
}
