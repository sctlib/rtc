import { css } from "lit";

export const peerStyles = css`
	:host {
		box-sizing: border-box;
	}
	*,
	*:before,
	*:after {
		box-sizing: inherit;
	}
	:host {
		font-size: var(--fs);
	}
	button {
		cursor: pointer;
		padding: calc(var(--size) / 3);
	}
	/* send data to peer */
	rtc-send-data form {
		display: flex;
		flex-direction: column;
	}
	rtc-send-data textarea {
		resize: vertical;
		min-height: calc(var(--size) * 3);
		background-color: var(--c-bg);
		color: var(--c-fg);
		padding: calc(var(--size) / 3);
	}
`;
