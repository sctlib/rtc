import { css } from "lit";

export const buttonStyles = css`
	button {
		cursor: pointer;
		padding: calc(var(--size) / 4);
	}
`;

export const inputStyles = css`
	input {
		padding: calc(var(--size) / 3);
	}
`;

export const formStyles = css`
	form {
		display: flex;
		flex-direction: column;
	}
	input,
	textarea {
		padding: calc(var(--size) / 3);
	}
	label {
		font-style: italic;
	}
`;
