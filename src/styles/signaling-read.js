import { css } from "lit";

const signalingPasteStyles = css`
	rtc-signaling-paste::part(input) {
		max-width: 8rem;
		transition: max-width 200ms ease-in-out;
	}
`;

const qrCodeScannerStyles = css`
	qr-code-scanner {
		display: flex;
		flex-direction: column;
		border: 0.1rem solid inherit;
		max-width: 20rem;
	}
	qr-code-scanner[scanning] {
	}
	qr-code-scanner::part(button-start),
	qr-code-scanner::part(button-stop) {
		cursor: pointer;
		padding: calc(var(--size) / 3);
		order: 1;
	}
	qr-code-scanner::part(button-start) {
	}
	qr-code-scanner::part(button-stop) {
		color: red;
		font-size: 1.3rem;
		flex-grow: 1;
	}
	qr-code-scanner::part(select-device) {
		padding: calc(var(--size) / 3);
		order: 3;
	}
	qr-code-scanner::part(video-control) {
		order: 2;
		background-color: var(--c-bg-scanner, black);
		width: 100%;
	}
	qr-code-scanner[scanning]::part(video-control) {
		filter: saturate(1);
		transition: filter 222ms ease-in-out;
		max-height: 30vh;
	}
	qr-code-scanner[scanning][success]::part(video-control) {
		filter: saturate(100);
	}
	qr-code-scanner[scanning]::before {
		pointer-events: none;
		content: "";
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: var(--c-bg, blue);
		z-index: -1;
	}
	qr-code-scanner[scanning][success]::before {
		background-color: var(--c-fg, black);
	}
`;

const hostStyles = css`
	:host {
		display: inline-flex;
		justify-content: center;
		align-items: center;
		flex-wrap: wrap;
	}
`;

export const signalingReadStyles = css`
	${hostStyles}
	${signalingPasteStyles}
	${qrCodeScannerStyles}
	qr-code-scanner[scanning] + rtc-signaling-paste {
		display: none;
	}
`;
