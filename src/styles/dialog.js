import { css } from "lit";

export const dialogStyles = css`
	form[method="dialog"] {
		display: flex;
		justify-content: flex-end;
		padding-bottom: 1rem;
	}
	button {
		padding: calc(var(--size) / 3);
		cursor: pointer;
	}
`;
