import { css } from "lit";

const menuStyles = css`
	:host::part(menu) {
		display: flex;
		flex-wrap: wrap;
		align-items: center;
		list-style: none;
		margin: 0;
		padding: 0;
	}
	:host:not([method])::part(copy),
	:host:not([method])::part(qr-code),
	:host:not([method])::part(qr-codes) {
		margin: calc(var(--size) / 5);
	}
`;

const qrCodeStyles = css`
	qr-codes,
	qr-code,
	qr-codes::part(qr-code) {
		display: inline-block;
		max-height: 100vh;
		max-width: 3rem;
	}
	qr-codes[fullscreen]::part(qr-code),
	qr-code[fullscreen] {
		cursor: zoom-in;
	}
	qr-codes:fullscreen {
		display: flex;
		align-items: center;
	}
	qr-codes:fullscreen::part(qr-code) {
		max-width: 100vw;
		width: 100%;
	}
	qr-code:fullscreen {
		display: flex;
		justify-content: center;
		align-items: center;
		cursor: zoom-out;
	}
	qr-codes::part(qr-code-svg),
	qr-code::part(svg) {
		width: 100%;
		height: 100%;
		max-height: 100vh;
		height: auto;
	}
	qr-codes::part(qr-code-img),
	qr-code::part(img) {
		width: auto;
		max-width: 100%;
	}
`;

const qrCodeForceFullscreenStyles = css`
	:host([method^="qr-code"]) qr-codes {
		cursor: zoom-in;
	}
	:host([method^="qr-code"]) qr-codes[force-fullscreen] {
		z-index: 1;
		position: fixed;
		width: 100vw;
		height: 100vh;
		top: 0;
		left: 0;
		max-width: 100%;
		display: flex;
		align-items: center;
		background-color: var(--c-bg-qrs, black);
		margin: 0;
		cursor: zoom-out;
	}
	qr-codes[force-fullscreen]::part(qr-code) {
		max-width: 100vw;
		width: 100%;
		margin: 0;
	}
`;

export const shareStyles = css`
	${menuStyles}
	${qrCodeStyles}
	${qrCodeForceFullscreenStyles}
`;
