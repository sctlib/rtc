// vite.config.js
import { resolve } from "path";
import { defineConfig } from "vite";
import recursive from "recursive-readdir";

const BASE_URL = process.env.VITE_BASE || "/";

const generateExampleInputFiles = async () => {
	const entriesDir = resolve("./examples/");
	const entries = await recursive(entriesDir);
	const inputFiles = entries
		.filter((entry) => entry.endsWith(".html"))
		.reduce((acc, entry) => {
			const inputFilePath = entry.replace(__dirname + "/", "");
			const inputName = inputFilePath.replace(".html", "").split("/").join("_");
			acc[inputName] = `${inputFilePath}`;
			return acc;
		}, {});
	return inputFiles;
};

const examples = await generateExampleInputFiles();

export default defineConfig({
	base: BASE_URL,
	publicDir: "assets",
	server: {
		host: true,
		port: 5173,
	},
	build: {
		rollupOptions: {
			input: {
				main: resolve("index.html"),
				...examples,
			},
			output: {
				dir: "dist-website",
			},
		},
		minify: false,
	},
});
