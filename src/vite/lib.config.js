import { resolve } from "path";
import { defineConfig } from "vite";
import pkg from "../../package.json";

export default defineConfig({
	appType: "mpa",
	base: "./",
	publicDir: "assets",
	build: {
		/* minify: true, */
		lib: {
			entry: resolve("src/index.js"),
			formats: ["es"],
			name: "rtcpeer",
			// the proper extensions will be added
			fileName: "rtc-peer",
		},
		rollupOptions: {
			output: {
				dir: "dist-lib",
			},
			external: Object.keys(pkg.peerDependencies || []),
		},
	},
});
