import { LitElement, html } from "lit";
import { buildShareUrl, MATRIX_EVENT_TYPE } from "./utils.js";
import { shareStyles } from "./styles/share.js";
import mwc from "@sctlib/mwc";
const { api: matrixApi } = mwc;

export default class RTCSignalingShare extends LitElement {
	static properties = {
		data: { type: Object },
		method: { type: String },
		dataFull: { type: String },
		dataSplitted: { type: Array },
		userPeer: { type: Object },
		matrixPeers: { type: Array },
	};

	get dataFull() {
		return buildShareUrl(this.data);
	}

	get dataSplitted() {
		return this.splitDataToShare(this.data);
	}

	splitDataToShare(peerBundle) {
		const candidates = peerBundle.candidates || [];
		delete peerBundle.candidates;

		// handle the rest of the keys
		const partialDataSplitted = Object.keys(peerBundle).map((bundleKey) => {
			const bundlePart = peerBundle[bundleKey];
			const data = {};
			data[bundleKey] = bundlePart;
			return buildShareUrl(data);
		});

		// if no "end-of-candidates signal", add it
		// https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/iceconnectionstatechange_event#state_transitions_as_negotiation_ends
		if (!candidates.find((c) => c.candidate === "")) {
			candidates.push({ candidate: "" });
		}

		// handle the each of the candidates
		const candidatesDataSplitted = candidates.map((candidate) => {
			return buildShareUrl(candidate);
		});

		return [...partialDataSplitted, ...candidatesDataSplitted];
	}

	constructor() {
		super();
		this.data = {};
		this.method = "";
		this.userPeer = {};
		this.matrixPeers = [];
	}

	updated() {
		if (this.method === "qr-codes-auto") {
			try {
				this.shadowRoot.querySelector("qr-codes").click();
			} catch (error) {
				console.warn("Error requesting fullscreen", error);
			}
		}
	}

	_onQRsClick({ target }) {
		if (target.getAttribute("force-fullscreen") === "true") {
			target.removeAttribute("force-fullscreen");
		} else {
			target.setAttribute("force-fullscreen", true);
		}
	}
	async _onMatrixSubmit({ detail }) {
		try {
			const res = await matrixApi.sendToDevice({
				user_id: detail,
				content: {
					rtc_data_url: this.dataFull,
				},
				event_type: MATRIX_EVENT_TYPE,
			});
		} catch (e) {
			console.error("Error calling matrix api", e);
		}
	}

	render() {
		switch (this.method) {
			case "copypaste":
				return this._renderCopy();
			case "qr-code":
				return this._renderQR();
			case "qr-codes":
				return this._renderQRs({ interval: 1000 });
			case "qr-codes-auto":
				return this._renderQRs({ interval: 500 });
			case "matrix-user-device":
				return this._renderMatrixDevice();
			default:
				return html``;
		}
	}

	_renderCopy() {
		return html`
			<rtc-signaling-copy
				data=${this.dataFull}
				part="copy"
			></rtc-signaling-copy>
		`;
	}
	_renderQR() {
		return html`
			<qr-code
				data=${this.dataFull}
				format="svg"
				module-size="5"
				fullscreen="true"
				part="qr-code"
				title="Share this peer connection bundle with a peer"
			></qr-code>
		`;
	}
	_renderQRs({ interval } = {}) {
		interval = interval > 333 ? interval : 1000;
		return html`
			<qr-codes
				?force-fullscreen=${true}
				interval=${interval}
				.data=${this.dataSplitted}
				@click=${this._onQRsClick}
				format="svg"
				module-size="5"
				title="Use the scanner on all these qr-codes for the full peer connection data. This app's scanner will signal you when the process is done."
				part="qr-codes"
			></qr-codes>
		`;
	}
	_renderMatrixDevice() {
		return html`<rtc-signaling-matrix-out
			.data=${this.dataFull}
			.userPeer=${this.userPeer}
			.usernames=${this.matrixPeers}
			@submit=${this._onMatrixSubmit}
			part="matrix"
		></rtc-signaling-matrix-out>`;
	}

	static styles = [shareStyles];
}
